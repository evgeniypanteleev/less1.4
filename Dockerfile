FROM debian:10 as build

RUN apt update \
	&& apt install -y \
	wget\	
	make\
	build-essential\
	libpcre3-dev\
	libpcre++-dev\
	perl\
	libssl-dev\
	zlib1g\
	zlib1g-dev

RUN cd /usr/local/src \
	&& wget 'https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.22rc1.tar.gz' \
	&& tar -xzvf v0.1.22rc1.tar.gz \
	&& cd lua-resty-core-0.1.22rc1 \
	&& make \
	&& make install

RUN cd /usr/local/src \
	&& wget 'https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.11rc1.tar.gz' \
	&& tar -xzvf v0.11rc1.tar.gz \
	&& cd lua-resty-lrucache-0.11rc1 \
	&& make \
	&& make install

RUN cd /usr/local/src \
	&& wget 'https://github.com/openresty/luajit2/archive/refs/tags/v2.1-20210510.tar.gz' \
	&& tar -xzvf v2.1-20210510.tar.gz \
	&& cd luajit2-2.1-20210510 \
	&& make \
	&& make install

RUN cd /usr/local/src \
	&& wget 'https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz' \
	&& tar -xzvf v0.3.1.tar.gz \
	&& wget 'https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.20rc1.tar.gz' \
	&& tar -xzvf v0.10.20rc1.tar.gz \
	&& wget 'https://openresty.org/download/nginx-1.19.3.tar.gz' \
	&& tar -xzvf nginx-1.19.3.tar.gz \
	&& cd nginx-1.19.3/ \
	&& export LUAJIT_LIB=/usr/lib \
	&& export LUAJIT_INC=/usr/local/include/luajit-2.1 \
	&& ./configure \
	--user=nobody \
	--group=nobody                         \
	--prefix=/etc/nginx                   \	
	--sbin-path=/usr/sbin/nginx           \
	--conf-path=/etc/nginx/nginx.conf     \
	--pid-path=/var/run/nginx.pid         \
	--lock-path=/var/run/nginx.lock       \
	--error-log-path=/var/log/nginx/error.log \
	--http-log-path=/var/log/nginx/access.log \
	--with-http_gzip_static_module        \
	--with-http_stub_status_module        \
	--with-http_ssl_module                \
	--with-pcre                           \
	--with-file-aio                       \
	--with-http_realip_module             \
	--without-http_scgi_module            \
	--without-http_uwsgi_module           \
	--without-http_fastcgi_module ${NGINX_DEBUG:+--debug} \
	--with-cc-opt=-O2 \
	--with-ld-opt='-Wl,-rpath,/usr/local/lib' \
	--add-dynamic-module=/usr/local/src/ngx_devel_kit-0.3.1 \
	--add-dynamic-module=/usr/local/src/lua-nginx-module-0.10.20rc1 \
	&& make \
	&& make install

FROM debian:10
WORKDIR /usr/sbin
ENV LUAJIT_LIB /usr/local/lib/
ENV LUAJIT_INC /usr/local/include/luajit-2.1
COPY --from=build /usr/sbin/nginx /usr/sbin/nginx
COPY --from=build /etc/nginx /etc/nginx
COPY --from=build /usr/lib /usr/lib
COPY --from=build /var/log/nginx /var/log/nginx
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/include/luajit-2.1 /usr/local/include/luajit-2.1
RUN mkdir ../logs ../conf && touch ../logs/error.log
CMD ["./nginx","-g","daemon off;"]
